<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$dir = "PID_GTFS";

$stops = fopen("$dir/stops.txt", 'r');
$i     = 0;
if ($stops) {
    $clean_stops_tab = mysqli_query($link, "TRUNCATE TABLE gtfsstops;");
    while (($buffer0 = fgets($stops, 4096)) !== false) {
        if ($i > 0) {
            $pos1     = strpos($buffer0, ',');
            $stop_id  = substr($buffer0, 0, $pos1);
            $newstart = $pos1 + 1;
            $buffer0  = substr($buffer0, $newstart);

            if (substr($buffer0, 0, 1) == '"') {
                $buffer0   = substr($buffer0, 1);
                $pos2      = strpos($buffer0, '"');
                $stop_name = substr($buffer0, 0, $pos2);
                $newstart  = $pos2 + 2;

                $buffer0  = substr($buffer0, $newstart);
                $string38 = $buffer0;

                $pos3            = strpos($buffer0, '"');
                $stop_coord_text = substr($buffer0, 0, $pos3);
                $coor_line       = explode(',', $buffer0);
                $stop_lat        = $coor_line[0];
                $stop_lon        = $coor_line[1];

                $stop_type = $coor_line[4];
                if ($stop_type == "") {
                    $stop_type = "0";
                }
                $stop_platform = trim($coor_line[8]);
            } else {
                $pos2      = strpos($buffer0, ',');
                $stop_name = substr($buffer0, 0, $pos2);
                $newstart  = $pos2 + 1;

                $buffer0  = substr($buffer0, $newstart);
                $string38 = $buffer0;

                $pos3            = strpos($buffer0, '"');
                $stop_coord_text = substr($buffer0, 0, $pos3);
                $coor_line       = explode(',', $buffer0);
                $stop_lat        = $coor_line[0];
                $stop_lon        = $coor_line[1];

                if (count($coor_line) == 10) {
                    $stop_type = $coor_line[5];
                    if ($stop_type == "") {
                        $stop_type = "0";
                    }
                    $stop_platform = trim($coor_line[9]);
                } else {
                    $stop_type = $coor_line[4];
                    if ($stop_type == "") {
                        $stop_type = "0";
                    }
                }
                $stop_platform = trim($coor_line[8]);
            }

            if ($stop_name == "" or $stop_name == "<Null>") {
                $pos4      = strpos($stop_id, 'E');
                $stop_name = substr($stop_id, $pos4);

            }

            if ($stop_type != 3) {
                $query37  = "INSERT INTO gtfsstops (stop_id,stop_name,stop_lat,stop_lon,stop_type,stop_platform) VALUES ('$stop_id', '$stop_name', '$stop_lat', '$stop_lon', '$stop_type', '$stop_platform');";
                $prikaz37 = mysqli_query($link, $query37);
                if (!$prikaz37) {
                    echo "Chyba stop_id: $stop_id - hlásí " . mysqli_error($link) . "<br/>";
                }
            }
        }
        $i = $i + 1;
    }
    fclose($stops);
}

echo "Hotovo...";
mysqli_close($link);
