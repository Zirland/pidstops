<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$dir = "PID_GTFS";

$routes = fopen("$dir/routes.txt", 'r');
$i      = 0;
if ($routes) {
    $clean_routes_tab = mysqli_query($link, "TRUNCATE TABLE routes;");
    while (($buffer0 = fgets($routes, 4096)) !== false) {
        if ($i > 0) {
            $route_line = explode('"', $buffer0);

            $route_data  = explode(',', $route_line[0]);
            $route_id    = $route_data[0];
            $route_short = $route_data[2];

            if (substr($route_line[1], 0, 3) == "htt") {
                $route_type = substr($route_line[0], -2, 1);

            } else {
                $route_type = substr($route_line[2], 1, -1);
            }

            $query154  = "INSERT INTO routes (route_id, route_short, route_type) VALUES ('$route_id', '$route_short', '$route_type');";
            $prikaz154 = mysqli_query($link, $query154);
            if (!$prikaz154) {
                echo "Chyba route_id: $route_id - hlásí " . mysqli_error($link) . "<br/>";
            }
        }
        $i = $i + 1;
    }
    fclose($routes);
}

echo "Hotovo...";
mysqli_close($link);
