<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$current = "id;name;platform;lat;lon;group;tramvaj;metro;vlak;autobus;privoz;trolejbus;gondola;lanovka;ikona\n";
$query14 = "SELECT id, `name`, platform, lat, lon, `group`, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM stopsout;";
if ($result14 = mysqli_query($link, $query14)) {
    while ($row14 = mysqli_fetch_row($result14)) {
        $id        = $row14[0];
        $name      = $row14[1];
        $platform  = $row14[2];
        $lat       = $row14[3];
        $lon       = $row14[4];
        $group     = $row14[5];
        $tramvaj   = $row14[6];
        $metro     = $row14[7];
        $vlak      = $row14[8];
        $autobus   = $row14[9];
        $privoz    = $row14[10];
        $trolejbus = $row14[11];
        $gondola   = $row14[12];
        $lanovka   = $row14[13];

        $current .= "$id;\"$name\";$platform;$lat;$lon;$group;$tramvaj;$metro;$vlak;$autobus;$privoz;$trolejbus;$gondola;$lanovka;\"i$tramvaj$metro$vlak$autobus$privoz$trolejbus$gondola$lanovka\"\n";
    }
    mysqli_free_result($result14);
}

$file = "VHD_stops_WGS_UTF8.csv";
file_put_contents($file, $current);

$current = "";
$query14 = "SELECT id, `name`, platform, lat, lon, `group`, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM xmlstops WHERE `group` = '1';";
if ($result14 = mysqli_query($link, $query14)) {
    while ($row14 = mysqli_fetch_row($result14)) {
        $id        = $row14[0];
        $name      = $row14[1];
        $platform  = $row14[2];
        $lat       = $row14[3];
        $lon       = $row14[4];
        $group     = $row14[5];
        $tramvaj   = $row14[6];
        $metro     = $row14[7];
        $vlak      = $row14[8];
        $autobus   = $row14[9];
        $privoz    = $row14[10];
        $trolejbus = $row14[11];
        $gondola   = $row14[12];
        $lanovka   = $row14[13];

        $new_id = "U".$id."S1";
        $current .= "$new_id;\"$name\";$platform;$lat;$lon;$group;$tramvaj;$metro;$vlak;$autobus;$privoz;$trolejbus;$gondola;$lanovka;\"i$tramvaj$metro$vlak$autobus$privoz$trolejbus$gondola$lanovka\"\n";
    }
    mysqli_free_result($result14);
}

$file = "VHD_stops_WGS_UTF8.csv";
file_put_contents($file, $current, FILE_APPEND);

$current = "";
$query14 = "SELECT id, `name`, platform, lat, lon, `group`, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM xmlstops WHERE `group` = '0';";
if ($result14 = mysqli_query($link, $query14)) {
    while ($row14 = mysqli_fetch_row($result14)) {
        $id        = $row14[0];
        $name      = $row14[1];
        $platform  = $row14[2];
        $lat       = $row14[3];
        $lon       = $row14[4];
        $group     = $row14[5];
        $tramvaj   = $row14[6];
        $metro     = $row14[7];
        $vlak      = $row14[8];
        $autobus   = $row14[9];
        $privoz    = $row14[10];
        $trolejbus = $row14[11];
        $gondola   = $row14[12];
        $lanovka   = $row14[13];

        $break_id = explode("/", $id);
        $new_id = "U".$break_id[0]."Z".$break_id[1];
        $current .= "$new_id;\"$name\";$platform;$lat;$lon;$group;$tramvaj;$metro;$vlak;$autobus;$privoz;$trolejbus;$gondola;$lanovka;\"i$tramvaj$metro$vlak$autobus$privoz$trolejbus$gondola$lanovka\"\n";
    }
    mysqli_free_result($result14);
}

$file = "VHD_stops_WGS_UTF8.csv";
file_put_contents($file, $current, FILE_APPEND);

echo "Done...";

mysqli_close($link);