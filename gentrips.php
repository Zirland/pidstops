<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$dir = "PID_GTFS";

$trips = fopen("$dir/trips.txt", 'r');
$i     = 0;
if ($trips) {
    $clean_trips_tab = mysqli_query($link, "TRUNCATE TABLE trips;");
    while (($buffer0 = fgets($trips, 4096)) !== false) {
        if ($i > 0) {
            $trip_line = explode(',', $buffer0);

            $route_id = $trip_line[0];
            $trip_id  = $trip_line[2];

            $query130  = "INSERT INTO trips (trip_id,route_id) VALUES ('$trip_id', '$route_id');";
            $prikaz130 = mysqli_query($link, $query130);
            if (!$prikaz130) {
                echo "Chyba trip_id: $trip_id - hlásí " . mysqli_error($link) . "<br/>";
            }
        }
        $i = $i + 1;
    }
    fclose($trips);
}

echo "Hotovo...";
mysqli_close($link);
