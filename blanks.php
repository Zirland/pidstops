<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<title>JDF</title>
	<script type="text/javascript" src="https://api.mapy.cz/loader.js"></script>
	<script type="text/javascript">
		Loader.lang = "cs";
		Loader.load(null, {
			poi: true
		});
	</script>
</head>

<body>
<table>
    <tr><th>P.Č.</th><th>Id</th><th>Název</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th></tr>

    <?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$action  = $_POST["action"];
$id_stop = $_POST["stop"];
$stop_T  = $_POST["T"];
if ($stop_T == '') {
    $stop_T = "0";
}
$stop_M = $_POST["M"];
if ($stop_M == '') {
    $stop_M = "0";
}
$stop_V = $_POST["V"];
if ($stop_V == '') {
    $stop_V = "0";
}
$stop_B = $_POST["B"];
if ($stop_B == '') {
    $stop_B = "0";
}
$stop_S = $_POST["S"];
if ($stop_S == '') {
    $stop_S = "0";
}
$stop_C = $_POST["C"];
if ($stop_C == '') {
    $stop_C = "0";
}
$stop_G = $_POST["G"];
if ($stop_G == '') {
    $stop_G = "0";
}
$stop_F = $_POST["F"];
if ($stop_F == '') {
    $stop_F = "0";
}

if ($action == "typ") {
    $query64  = "UPDATE xmlstops SET linky = '~', tramvaj = '$stop_T', metro = '$stop_M', vlak = '$stop_V', autobus = '$stop_B', privoz = '$stop_S', trolejbus = '$stop_C', gondola = '$stop_G', lanovka = '$stop_F' WHERE id = '$id_stop';";
    $prikaz64 = mysqli_query($link, $query64);
}

$query6 = "SELECT id FROM xmlstops WHERE linky = '' LIMIT 1;";
if ($result6 = mysqli_query($link, $query6)) {
    while ($row6 = mysqli_fetch_row($result6)) {
        $id = $row6[0];

        $id_explode = explode("/", $id);
        $id_root    = $id_explode[0];

        $i       = 1;
        $query14 = "SELECT id, name, linky, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM xmlstops WHERE id LIKE '$id_root/%';";
        if ($result14 = mysqli_query($link, $query14)) {
            while ($row14 = mysqli_fetch_row($result14)) {
                $stop_id        = $row14[0];
                $stop_name      = $row14[1];
                $stop_linky     = $row14[2];
                $stop_tramvaj   = $row14[3];
                $stop_metro     = $row14[4];
                $stop_vlak      = $row14[5];
                $stop_autobus   = $row14[6];
                $stop_privoz    = $row14[7];
                $stop_trolejbus = $row14[8];
                $stop_gondola   = $row14[9];
                $stop_lanovka   = $row14[10];

                echo "<tr><td>$i</td><td>$stop_id</td><td>$stop_name";
                if ($stop_linky == '') {
                    echo "<form method=\"post\" action=\"blanks.php\">
                    <input type=\"hidden\" name=\"action\" value=\"typ\">
                    <input type=\"hidden\" name=\"stop\" value=\"$stop_id\">";
                }
                echo "</td>";

                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"T\" value=\"1\">";
                } else {
                    if ($stop_tramvaj == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"M\" value=\"1\">";
                } else {
                    if ($stop_metro == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"V\" value=\"1\">";
                } else {
                    if ($stop_vlak == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"B\" value=\"1\">";
                } else {
                    if ($stop_autobus == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"S\" value=\"1\">";
                } else {
                    if ($stop_privoz == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"C\" value=\"1\">";
                } else {
                    if ($stop_trolejbus == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"G\" value=\"1\">";
                } else {
                    if ($stop_gondola == "1") {
                        echo "X";
                    }
                }
                echo "</td>";
                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"checkbox\" name=\"F\" value=\"1\">";
                } else {
                    if ($stop_lanovka == "1") {
                        echo "X";
                    }
                }
                echo "</td>";

                echo "<td>";
                if ($stop_linky == '') {
                    echo "<input type=\"submit\"></form>";
                }
                echo "</td></tr>";

                $i = $i + 1;
            }
        }

    }
}
?>
</table>

<div id="m" style="height:600px"></div>

<script type="text/javascript">
	function addMarker(nazev, id, x, y) {
		var znacka = JAK.mel("div");
		var obrazek = JAK.mel("img", {
			src: SMap.CONFIG.img + "/marker/drop-red.png"
		});
		znacka.appendChild(obrazek);

		var popisek = JAK.mel("div", {}, {
			position: "absolute",
			left: "0px",
			top: "2px",
			textAlign: "center",
			width: "22px",
			color: "white",
			fontWeight: "bold"
		});
		popisek.innerHTML = nazev;
		znacka.appendChild(popisek);


		var options = {
			title: nazev,
			url: znacka
		};

		var pozice = SMap.Coords.fromWGS84(Number(x), Number(y));
		var marker = new SMap.Marker(pozice, id, options);
		marker.decorate(SMap.Marker.Feature.Draggable);
		vrstva.addMarker(marker);
		markers.push(pozice);
	}

	var m = new SMap(JAK.gel("m"));
	m.addDefaultLayer(SMap.DEF_BASE).enable();

	m.addControl(new SMap.Control.Sync());
	var mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM);
	m.addControl(mouse);

	var vrstva = new SMap.Layer.Marker();
	m.addLayer(vrstva);
	vrstva.enable();
	var markers = [];

	<?php
$query106 = "SELECT id FROM xmlstops WHERE linky = '' LIMIT 1;";
if ($result106 = mysqli_query($link, $query106)) {
    while ($row106 = mysqli_fetch_row($result106)) {
        $id = $row106[0];

        $id_explode = explode("/", $id);
        $id_root    = $id_explode[0];

        $i        = 1;
        $query114 = "SELECT id, name, lat, lon FROM xmlstops WHERE id LIKE '$id_root/%';";
        if ($result114 = mysqli_query($link, $query114)) {
            while ($row114 = mysqli_fetch_row($result114)) {
                $stop_id   = $row114[0];
                $stop_name = $row114[1];
                $stop_lat  = $row114[2];
                $stop_lon  = $row114[3];

                echo "addMarker($i, $i, $stop_lon, $stop_lat);\n";
                $i = $i + 1;
            }
        }

    }
}
?>

    var layer2 = new SMap.Layer.Marker(undefined, {
		poiTooltip: true
	});
	m.addLayer(layer2).enable();

	var dataProvider = m.createDefaultDataProvider();
	dataProvider.setOwner(m);
	dataProvider.addLayer(layer2);
	dataProvider.setMapSet(SMap.MAPSET_BASE);
	dataProvider.enable();

    var cz = m.computeCenterZoom(markers);
	m.setCenterZoom(cz[0], cz[1]);
</script>
