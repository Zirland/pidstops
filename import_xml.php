<?php
ini_set('max_execution_time', 0);

require 'vendor/autoload.php';
require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$streamer = Prewk\XmlStringStreamer::createStringWalkerParser("StopsByName.xml");

$clean_xml_tab = mysqli_query($link, "TRUNCATE TABLE xmlstops;");

while ($group = $streamer->getNode()) {
    $simpleGroup = simplexml_load_string($group);
    $grouplinky  = '';
    unset($grouplinkyArr);
    $grouptramvaj = $groupmetro = $groupvlak = $groupautobus = $groupprivoz = $grouptrolejbus = $groupgondola = $grouplanovka = 0;

    foreach ($simpleGroup->stop as $simplestop) {
        $stopId   = $simplestop->attributes()->id;
        $stopName = $simplestop->attributes()->altIdosName;
        $stopLat  = $simplestop->attributes()->lat;
        $stopLon  = $simplestop->attributes()->lon;
        $platform = $simplestop->attributes()->platform;
        $group    = 0;
        $linky    = '';
        unset($linkyArr);
        $tramvaj = $metro = $vlak = $autobus = $privoz = $trolejbus = $gondola = $lanovka = 0;

        foreach ($simplestop->line as $simpleline) {
            $lineName        = $simpleline->attributes()->name;
            $lineType        = $simpleline->attributes()->type;
            $linkyArr[]      = trim($lineName);
            $grouplinkyArr[] = trim($lineName);
            switch ($lineType) {
                case 'tram':
                    $tramvaj      = 1;
                    $grouptramvaj = 1;
                    break;

                case 'metro':
                    $metro      = 1;
                    $groupmetro = 1;
                    break;

                case 'train':
                    $vlak      = 1;
                    $groupvlak = 1;
                    break;

                case 'bus':
                    $autobus      = 1;
                    $groupautobus = 1;
                    break;

                case 'ferry':
                    $privoz      = 1;
                    $groupprivoz = 1;
                    break;

                case 'trolleybus':
                    $trolejbus      = 1;
                    $grouptrolejbus = 1;
                    break;

                case 'funicular':
                    $lanovka      = 1;
                    $grouplanovka = 1;
                    break;
            }
        }

        if (isset($linkyArr)) {
            $linkyArr = array_unique($linkyArr);
            sort($linkyArr);
            $linky = implode(", ", $linkyArr);
        }

        $query87  = "INSERT INTO xmlstops (id, `name`, platform, lat, lon, `group`, linky, tramvaj, metro, vlak,autobus, privoz, trolejbus, gondola, lanovka) VALUES ('$stopId','$stopName','$platform','$stopLat','$stopLon','$group','$linky','$tramvaj','$metro','$vlak','$autobus','$privoz','$trolejbus','$gondola','$lanovka');";
        $prikaz87 = mysqli_query($link, $query87);
        if (!$prikaz87) {
            echo "Chyba stopId: $stopId - hlásí " . mysqli_error($link) . "<br/>";
        }
    }

    $groupId   = $simpleGroup->attributes()->node;
    $groupName = $simpleGroup->attributes()->idosName;
    $groupLat  = $simpleGroup->attributes()->avgLat;
    $groupLon  = $simpleGroup->attributes()->avgLon;
    $group     = 1;

    if (isset($grouplinkyArr)) {
        $grouplinkyArr = array_unique($grouplinkyArr);
        sort($grouplinkyArr);
        $grouplinky = implode(", ", $grouplinkyArr);
    }
    $query104  = "INSERT INTO xmlstops (id, `name`, platform, lat, lon, `group`, linky, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka) VALUES ('$groupId','$groupName','','$groupLat','$groupLon','$group','$grouplinky','$grouptramvaj','$groupmetro','$groupvlak','$groupautobus','$groupprivoz','$grouptrolejbus','$groupgondola','$grouplanovka');";
    $prikaz104 = mysqli_query($link, $query104);
    if (!$prikaz104) {
        echo "Chyba groupId: $groupId - hlásí " . mysqli_error($link) . "<br/>";
    }

}

echo "Hotovo...";

mysqli_close($link);