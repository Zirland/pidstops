<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<title>JDF</title>
	<script type="text/javascript" src="https://api.mapy.cz/loader.js"></script>
	<script type="text/javascript">
		Loader.lang = "cs";
		Loader.load(null, {
			poi: true
		});
	</script>
</head>

<body>

<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

if ($_POST['action'] == 'Merge') {
    $maxitems = $_POST['items'];
    unset($itemlist);

    unset($new_stop_id);
    unset($new_stop_name);
    unset($new_stop_platform);
    unset($new_stop_lat);
    unset($new_stop_lon);
    unset($new_stop_group);
    $new_stop_tramvaj   = 0;
    $new_stop_metro     = 0;
    $new_stop_vlak      = 0;
    $new_stop_autobus   = 0;
    $new_stop_privoz    = 0;
    $new_stop_trolejbus = 0;
    $new_stop_gondola   = 0;
    $new_stop_lanovka   = 0;

    for ($j = 1; $j < $maxitems; $j++) {
        $index      = "item" . $j;
        $itemlist[] = $_POST[$index];
    }

    $itemlist = array_filter($itemlist);
    foreach ($itemlist as $itemcode) {
        $code = substr($itemcode, 1);
        switch (substr($itemcode, 0, 1)) {
            case "X":
                $query41 = "SELECT id, `name`, platform, lat, lon, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka, `group` FROM xmlstops WHERE id = '$code';";
                if ($result41 = mysqli_query($link, $query41)) {
                    while ($row41 = mysqli_fetch_row($result41)) {
                        $new_stop_id[]       = $row41[0];
                        $new_stop_name[]     = $row41[1];
                        $new_stop_platform[] = $row41[2];
                        $new_stop_lat[]      = $row41[3];
                        $new_stop_lon[]      = $row41[4];
                        $new_stop_tramvaj += $row41[5];
                        $new_stop_metro += $row41[6];
                        $new_stop_vlak += $row41[7];
                        $new_stop_autobus += $row41[8];
                        $new_stop_privoz += $row41[9];
                        $new_stop_trolejbus += $row41[10];
                        $new_stop_gondola += $row41[11];
                        $new_stop_lanovka += $row41[12];
                        $new_stop_group[] = $row41[13];
                    }
                }
                break;
            case "G":
                $query120 = "SELECT stop_id, stop_name, stop_platform, stop_lat, stop_lon, stop_type FROM gtfsstops WHERE stop_id = '$code';";
                if ($result120 = mysqli_query($link, $query120)) {
                    while ($row120 = mysqli_fetch_row($result120)) {
                        $new_stop_id[]       = $row120[0];
                        $new_stop_name[]     = $row120[1];
                        $new_stop_platform[] = $row120[2];
                        $new_stop_lat[]      = $row120[3];
                        $new_stop_lon[]      = $row120[4];
                        $new_stop_group[]    = $row120[5];

                        unset($stop_druhy);
                        $query134 = "SELECT route_type FROM routes WHERE route_id IN (
                                SELECT DISTINCT route_id FROM trips WHERE trip_id IN (
                                    SELECT DISTINCT trip_id FROM stoptimes WHERE stop_id = '$code'
                                )
                            );";
                        if ($result134 = mysqli_query($link, $query134)) {
                            while ($row134 = mysqli_fetch_row($result134)) {
                                $stop_druhy[] = $row134[0];
                            }
                        }

                        $stop_druhy = array_unique($stop_druhy);
                        sort($stop_druhy);

                        if (in_array("0", $stop_druhy)) {
                            $new_stop_tramvaj += 1;
                        }

                        if (in_array("1", $stop_druhy)) {
                            $new_stop_metro += 1;
                        }

                        if (in_array("2", $stop_druhy)) {
                            $new_stop_vlak += 1;
                        }

                        if (in_array("3", $stop_druhy)) {
                            $new_stop_autobus += 1;
                        }

                        if (in_array("4", $stop_druhy)) {
                            $new_stop_privoz += 1;
                        }

                        if (in_array("5", $stop_druhy)) {
                            $new_stop_trolejbus += 1;
                        }

                        if (in_array("6", $stop_druhy)) {
                            $new_stop_gondola += 1;
                        }

                        if (in_array("7", $stop_druhy)) {
                            $new_stop_lanovka += 1;
                        }
                        break;
                    }

                }
        }
    }

    $new_stop_id       = array_unique($new_stop_id);
    $new_stop_name     = array_unique($new_stop_name);
    $new_stop_platform = array_unique($new_stop_platform);
    $new_stop_group    = array_unique($new_stop_group);

    echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
    $i = 0;
    foreach ($itemlist as $itemcode) {
        $i = $i + 1;
        echo "<input type=\"hidden\" name=\"item$i\" value=\"$itemcode\">";
    }
    echo "<input type=\"hidden\" name=\"items\" value=\"$i\">";

    echo "<table>";
    echo "<tr><th>Id</th><th>Název</th><th>Platform</th><th>Lat</th><th>Lon</th><th>Group</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th></tr>";
    echo "<tr>";
    echo "<td>";
    foreach ($new_stop_id as $cand_id) {
        echo "<input type=\"radio\" name=\"finalid\" value=\"$cand_id\" CHECKED>";
        echo "<label for=\"$cand_id\">$cand_id</label><br/>";
    }
    echo "</td>";
    echo "<td>";
    foreach ($new_stop_name as $cand_name) {
        $cand_name = str_replace(",", ", ", $cand_name);
        $cand_name = str_replace(".", ". ", $cand_name);
        $cand_name = str_replace("  ", " ", $cand_name);
        $cand_name = str_replace(". ,", ".,", $cand_name);
        $cand_name = str_replace(". )", ".)", $cand_name);

        echo "<input type=\"radio\" name=\"finalname\" value=\"$cand_name\" CHECKED>";
        echo "<label for=\"$cand_name\">$cand_name</label><br/>";
    }
    echo "</td>";
    echo "<td>";
    foreach ($new_stop_platform as $cand_platform) {
        echo "<input type=\"radio\" name=\"finalplatform\" value=\"$cand_platform\" CHECKED>";
        echo "<label for=\"$cand_platform\">$cand_platform</label><br/>";
    }
    echo "</td>";
    echo "<td>";
    $mid_lat = (min($new_stop_lat) + max($new_stop_lat)) / 2;
    echo "<input type=\"text\" id=\"latitude\" name=\"finallat\" value=\"$mid_lat\">";
    echo "</td>";
    echo "<td>";
    $mid_lon = (min($new_stop_lon) + max($new_stop_lon)) / 2;
    echo "<input type=\"text\" id=\"longitude\" name=\"finallon\" value=\"$mid_lon\">";
    echo "</td>";

    echo "<td>";
    foreach ($new_stop_group as $cand_group) {
        echo "<input type=\"radio\" name=\"finalgroup\" value=\"$cand_group\" CHECKED>";
        echo "<label for=\"$cand_group\">$cand_group</label><br/>";
    }

    echo "<td>";
    echo "<input type=\"checkbox\" name=\"T\" value=\"1\"";
    if ($new_stop_tramvaj > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"M\" value=\"1\"";
    if ($new_stop_metro > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"V\" value=\"1\"";
    if ($new_stop_vlak > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"B\" value=\"1\"";
    if ($new_stop_autobus > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"S\" value=\"1\"";
    if ($new_stop_privoz > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"C\" value=\"1\"";
    if ($new_stop_trolejbus > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"G\" value=\"1\"";
    if ($new_stop_gondola > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"F\" value=\"1\"";
    if ($new_stop_lanovka > 0) {
        echo " CHECKED";
    }
    echo "></td>";

    echo "</tr>";
    echo "</table>";
    echo "<input type=\"submit\" name=\"action\" value=\"Confirm\"></form>";

    echo "<div id=\"m\" style=\"height:0px\"></div>";
    echo "<div id=\"m2\" style=\"height:0px\"></div>";
    echo "<div id=\"m3\" style=\"height:600px\"></div>";

} else if ($_POST['action'] == 'Confirm') {
    $final_stop_id       = $_POST["finalid"];
    $final_stop_name     = $_POST["finalname"];
    $final_stop_platform = $_POST["finalplatform"];
    $final_stop_group    = $_POST["finalgroup"];
    $final_lat           = $_POST["finallat"];
    $final_lon           = $_POST["finallon"];

    $maxitems = $_POST['items'];
    unset($itemlist);

    for ($j = 1; $j <= $maxitems; $j++) {
        $index      = "item" . $j;
        $itemlist[] = $_POST[$index];
    }

    $stop_T = $_POST["T"];
    if ($stop_T == '') {
        $stop_T = "0";
    }
    $stop_M = $_POST["M"];
    if ($stop_M == '') {
        $stop_M = "0";
    }
    $stop_V = $_POST["V"];
    if ($stop_V == '') {
        $stop_V = "0";
    }
    $stop_B = $_POST["B"];
    if ($stop_B == '') {
        $stop_B = "0";
    }
    $stop_S = $_POST["S"];
    if ($stop_S == '') {
        $stop_S = "0";
    }
    $stop_C = $_POST["C"];
    if ($stop_C == '') {
        $stop_C = "0";
    }
    $stop_G = $_POST["G"];
    if ($stop_G == '') {
        $stop_G = "0";
    }
    $stop_F = $_POST["F"];
    if ($stop_F == '') {
        $stop_F = "0";
    }

    $query401  = "INSERT INTO stopsout (id,`name`,platform,lat,lon,`group`,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka) VALUES ('$final_stop_id', '$final_stop_name', '$final_stop_platform', '$final_lat', '$final_lon', '$final_stop_group', '$stop_T', '$stop_M', '$stop_V', '$stop_B', '$stop_S', '$stop_C', '$stop_G', '$stop_F');";
    $prikaz401 = mysqli_query($link, $query401);
    if (!$prikaz401) {
        echo "Chyba - hlásí " . mysqli_error($link) . "<br/>";
    } else {
        $itemlist = array_filter($itemlist);
        foreach ($itemlist as $itemcode) {
            $code = substr($itemcode, 1);
            switch (substr($itemcode, 0, 1)) {
                case "X":
                    $query41  = "DELETE FROM xmlstops WHERE id = '$code';";
                    $prikaz41 = mysqli_query($link, $query41);
                    break;
                case "G":
                    $query120  = "DELETE FROM gtfsstops WHERE stop_id = '$code';";
                    $prikaz120 = mysqli_query($link, $query120);
                    break;
            }
        }
        header("location: compare.php");
    }
} else if ($_POST['action'] == 'Delete') {
    $maxitems = $_POST['items'];
    unset($itemlist);

    for ($j = 1; $j <= $maxitems; $j++) {
        $index      = "item" . $j;
        $itemlist[] = $_POST[$index];
    }

    $itemlist = array_filter($itemlist);
    foreach ($itemlist as $itemcode) {
        $code = substr($itemcode, 1);
        switch (substr($itemcode, 0, 1)) {
            case "X":
                $query41  = "DELETE FROM xmlstops WHERE id = '$code';";
                $prikaz41 = mysqli_query($link, $query41);
                break;
            case "G":
                $query120  = "DELETE FROM gtfsstops WHERE stop_id = '$code';";
                $prikaz120 = mysqli_query($link, $query120);
                break;
        }
    }
    header("location: compare.php");
} else if ($_POST['action'] == 'Group') {
    $maxitems = $_POST['items'];
    unset($itemlist);

    unset($new_stop_id);
    unset($new_stop_name);
    unset($new_stop_platform);
    unset($new_stop_lat);
    unset($new_stop_lon);
    unset($new_stop_group);
    $new_stop_tramvaj   = 0;
    $new_stop_metro     = 0;
    $new_stop_vlak      = 0;
    $new_stop_autobus   = 0;
    $new_stop_privoz    = 0;
    $new_stop_trolejbus = 0;
    $new_stop_gondola   = 0;
    $new_stop_lanovka   = 0;

    for ($j = 1; $j < $maxitems; $j++) {
        $index      = "item" . $j;
        $itemlist[] = $_POST[$index];
    }

    $itemlist = array_filter($itemlist);
    foreach ($itemlist as $itemcode) {
        $query120 = "SELECT id, `name`, lat, lon, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM stopsout WHERE id = '$itemcode';";
        if ($result120 = mysqli_query($link, $query120)) {
            while ($row120 = mysqli_fetch_row($result120)) {
                $new_stop_id[]   = $row120[0];
                $new_stop_name[] = $row120[1];
                $new_stop_lat[]  = $row120[2];
                $new_stop_lon[]  = $row120[3];
                $new_stop_tramvaj += $row120[4];
                $new_stop_metro += $row120[5];
                $new_stop_vlak += $row120[6];
                $new_stop_autobus += $row120[7];
                $new_stop_privoz += $row120[8];
                $new_stop_trolejbus += $row120[9];
                $new_stop_gondola += $row120[10];
                $new_stop_lanovka += $row120[11];
            }
        }
    }

    $new_stop_id   = array_unique($new_stop_id);
    $new_stop_name = array_unique($new_stop_name);

    echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
    $i = 0;
    foreach ($itemlist as $itemcode) {
        $i = $i + 1;
        echo "<input type=\"hidden\" name=\"item$i\" value=\"$itemcode\">";
    }
    echo "<input type=\"hidden\" name=\"items\" value=\"$i\">";

    echo "<table>";
    echo "<tr><th>Id</th><th>Název</th><th>Lat</th><th>Lon</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th><th>List</th></tr>";
    echo "<tr>";
    echo "<td>";
    $stop_id_arr = explode("Z", $new_stop_id[0]);
    $uzel        = $stop_id_arr[0];
    $poradi      = 1;
    $regxp       = $uzel . "S%";
    $query510    = "SELECT id FROM stopsout WHERE id LIKE '$regxp';";
    if ($result510 = mysqli_query($link, $query510)) {
        $poradi = $poradi + mysqli_num_rows($result510);
    }
    $uzel_id = $uzel . "S" . $poradi;
    echo "<input type=\"text\" name=\"finalid\" value=\"$uzel_id\">";
    echo "</td>";
    echo "<td>";
    foreach ($new_stop_name as $cand_name) {
        echo "<input type=\"radio\" name=\"finalname\" value=\"$cand_name\" CHECKED>";
        echo "<label for=\"$cand_name\">$cand_name</label><br/>";
    }
    echo "</td>";
    echo "<td>";
    $mid_lat = (min($new_stop_lat) + max($new_stop_lat)) / 2;
    echo "<input type=\"text\" id=\"latitude\" name=\"finallat\" value=\"$mid_lat\">";
    echo "</td>";
    echo "<td>";
    $mid_lon = (min($new_stop_lon) + max($new_stop_lon)) / 2;
    echo "<input type=\"text\" id=\"longitude\" name=\"finallon\" value=\"$mid_lon\">";
    echo "</td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"T\" value=\"1\"";
    if ($new_stop_tramvaj > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"M\" value=\"1\"";
    if ($new_stop_metro > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"V\" value=\"1\"";
    if ($new_stop_vlak > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"B\" value=\"1\"";
    if ($new_stop_autobus > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"S\" value=\"1\"";
    if ($new_stop_privoz > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"C\" value=\"1\"";
    if ($new_stop_trolejbus > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"G\" value=\"1\"";
    if ($new_stop_gondola > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    echo "<input type=\"checkbox\" name=\"F\" value=\"1\"";
    if ($new_stop_lanovka > 0) {
        echo " CHECKED";
    }
    echo "></td>";
    echo "<td>";
    $grouplist = implode(",", $itemlist);
    echo "<input type=\"text\" name=\"grouplist\" value=\"$grouplist\">";
    echo "</td>";

    echo "</tr>";
    echo "</table>";

    echo "<input type=\"submit\" name=\"action\" value=\"Skupina\">";
    echo "</form>";
} else if ($_POST['action'] == 'Skupina') {
    $final_stop_id   = $_POST["finalid"];
    $final_stop_name = $_POST["finalname"];
    $final_lat       = $_POST["finallat"];
    $final_lon       = $_POST["finallon"];

    $maxitems = $_POST['items'];
    unset($itemlist);

    for ($j = 1; $j <= $maxitems; $j++) {
        $index      = "item" . $j;
        $itemlist[] = $_POST[$index];
    }

    $stop_T = $_POST["T"];
    if ($stop_T == '') {
        $stop_T = "0";
    }
    $stop_M = $_POST["M"];
    if ($stop_M == '') {
        $stop_M = "0";
    }
    $stop_V = $_POST["V"];
    if ($stop_V == '') {
        $stop_V = "0";
    }
    $stop_B = $_POST["B"];
    if ($stop_B == '') {
        $stop_B = "0";
    }
    $stop_S = $_POST["S"];
    if ($stop_S == '') {
        $stop_S = "0";
    }
    $stop_C = $_POST["C"];
    if ($stop_C == '') {
        $stop_C = "0";
    }
    $stop_G = $_POST["G"];
    if ($stop_G == '') {
        $stop_G = "0";
    }
    $stop_F = $_POST["F"];
    if ($stop_F == '') {
        $stop_F = "0";
    }

    $stop_list = $_POST["grouplist"];

    $query401  = "INSERT INTO stopsout (id,`name`,platform,lat,lon,`group`,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka,`stoplist`) VALUES ('$final_stop_id', '$final_stop_name', '', '$final_lat', '$final_lon', '1', '$stop_T', '$stop_M', '$stop_V', '$stop_B', '$stop_S', '$stop_C', '$stop_G', '$stop_F', '$stop_list');";
    $prikaz401 = mysqli_query($link, $query401);
    if (!$prikaz401) {
        echo "Chyba - hlásí " . mysqli_error($link) . "<br/>";
    } else {
        header("location: compare.php");
    }
} else {
    echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
    echo "<table width=\"100%\"><tr>";
    echo "<td width=\"50%\">";

    unset($pozice);
    $mid_lon = 0;
    $mid_lat = 0;

    echo "<table>
    <tr><th>P.Č.</th><th></th><th>Id</th><th>Název</th><th>Nástupiště</th><th>Latitude</th><th>Longitude</th><th>Group</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th></tr>";

    $query36 = "SELECT id FROM xmlstops WHERE `group` = '1'  AND metro='1' ORDER BY `name` LIMIT 1;";  // Všechno metro
    if ($result36 = mysqli_query($link, $query36)) {
        $radky36 = mysqli_num_rows($result36);
        while ($row36 = mysqli_fetch_row($result36)) {
            $group_id = $row36[0];
        }
    }
    if ($radky36 == 0) {
        // Duplicitní uzly
        $query36 = "select tbl.id, tbl.pocet from (select id, count(*) as pocet from xmlstops where `group` = '1' group by id) as tbl where tbl.pocet > 1 order by tbl.id LIMIT 1;";
        if ($result36 = mysqli_query($link, $query36)) {
            $radky36 = mysqli_num_rows($result36);
            while ($row36 = mysqli_fetch_row($result36)) {
                $group_id = $row36[0];
            }
        }
    }
    if ($radky36 == 0) {
        // Uzly bez zastávky
        unset($children);
        $query557 = "select id from xmlstops where `group` = '0';";
        if ($result557 = mysqli_query($link, $query557)) {
            while ($row557 = mysqli_fetch_row($result557)) {
                $id_list    = $row557[0];
                $id_explode = explode("/", $id_list);
                $children[] = $id_explode[0];
            }
        }
        $children   = array_filter($children);
        $child_text = implode("','", $children);

        $query36 = "select id from xmlstops where `group` = '1' and id not in ('$child_text') LIMIT 1;";
        if ($result36 = mysqli_query($link, $query36)) {
            $radky36 = mysqli_num_rows($result36);
            while ($row36 = mysqli_fetch_row($result36)) {
                $group_id = $row36[0];
            }
        }
    }
    if ($radky36 == 0) {
        // Zastávky bez skupiny
        unset($children);
        $query557 = "SELECT stoplist FROM stopsout;";
        if ($result557 = mysqli_query($link, $query557)) {
            while ($row557 = mysqli_fetch_row($result557)) {
                $id_list    = $row557[0];
                $id_explode = explode(",", $id_list);
                foreach ($id_explode as $child_id) {
                    $children[] = $child_id;
                }
            }
        }
        $children   = array_filter($children);
        $child_text = implode("','", $children);
        $query36    = "SELECT id FROM stopsout where `group`='0' and id NOT IN ('$child_text');";
        if ($result36 = mysqli_query($link, $query36)) {
            $radky36 = mysqli_num_rows($result36);
            while ($row36 = mysqli_fetch_row($result36)) {
                $id         = $row36[0];
                $id_explode = explode("Z", $id);
                $group_id   = substr($id_explode[0], 1);
            }
        }
    }
    if ($radky36 == 0) {
        // Zastávky bez linky
        $query36 = "SELECT id FROM xmlstops WHERE linky = '' ORDER BY id LIMIT 1;";
        if ($result36 = mysqli_query($link, $query36)) {
            $radky36 = mysqli_num_rows($result36);
            while ($row36 = mysqli_fetch_row($result36)) {
                $id         = $row36[0];
                $id_explode = explode("/", $id);
                $group_id   = $id_explode[0];
            }
        }
    }
    if ($radky36 == 0) {
        // Duplicitní zastávky
        unset($dupes);
        $prev_id = $prev_name = $prev_platform = $prev_lat = $prev_lon = $prev_linky = $prev_tramvaj = $prev_metro = $prev_vlak = $prev_autobus = $prev_privoz = $prev_trolejbus = $prev_gondola = $prev_lanovka = "";
        $query6  = "SELECT id, name, platform, lat, lon, linky, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM xmlstops WHERE `group`= 0 ORDER BY lat, lon, name DESC, linky DESC;";
        if ($result6 = mysqli_query($link, $query6)) {
            while ($row6 = mysqli_fetch_row($result6)) {
                $id        = $row6[0];
                $name      = $row6[1];
                $platform  = $row6[2];
                $lat       = $row6[3];
                $lon       = $row6[4];
                $linky     = $row6[5];
                $tramvaj   = $row6[6];
                $metro     = $row6[7];
                $vlak      = $row6[8];
                $autobus   = $row6[9];
                $privoz    = $row6[10];
                $trolejbus = $row6[11];
                $gondola   = $row6[12];
                $lanovka   = $row6[13];

                if ($lat == $prev_lat && $lon == $prev_lon && ($linky == $prev_linky || $linky == '')) {
                    $id_explode = explode("/", $id);
                    $dupes[]    = $id_explode[0];
                } elseif ($lat == $prev_lat && $lon == $prev_lon) {
                    $nove_linky1 = explode(",", $prev_linky);
                    $nove_linky2 = explode(",", $linky);

                    $nove_linky = array_merge($nove_linky1, $nove_linky2);
                    $nove_linky = array_unique($nove_linky);
                    sort($nove_linky);
                    $nove_linkyTxt = implode(", ", $nove_linky);

                    $new_tramvaj = $tramvaj || $prev_tramvaj;
                    if ($new_tramvaj == false) {
                        $new_tramvaj = "0";
                    }
                    $new_metro = $metro || $prev_metro;
                    if ($new_metro == false) {
                        $new_metro = "0";
                    }
                    $new_vlak = $vlak || $prev_vlak;
                    if ($new_vlak == false) {
                        $new_vlak = "0";
                    }
                    $new_autobus = $autobus || $prev_autobus;
                    if ($new_autobus == false) {
                        $new_autobus = "0";
                    }
                    $new_privoz = $privoz || $prev_privoz;
                    if ($new_privoz == false) {
                        $new_privoz = "0";
                    }
                    $new_trolejbus = $trolejbus || $prev_trolejbus;
                    if ($new_trolejbus == false) {
                        $new_trolejbus = "0";
                    }
                    $new_gondola = $gondola || $prev_gondola;
                    if ($new_gondola == false) {
                        $new_gondola = "0";
                    }
                    $new_lanovka = $lanovka || $prev_lanovka;
                    if ($new_lanovka == false) {
                        $new_lanovka = "0";
                    }

                    $prev_id_explode = explode("/", $prev_id);
                    $dupes[]         = $prev_id_explode[0];
                    $id_explode      = explode("/", $id);
                    $dupes[]         = $id_explode[0];
                }

                $prev_id        = $id;
                $prev_name      = $name;
                $prev_platform  = $platform;
                $prev_lat       = $lat;
                $prev_lon       = $lon;
                $prev_linky     = $linky;
                $prev_tramvaj   = $tramvaj;
                $prev_metro     = $metro;
                $prev_vlak      = $vlak;
                $prev_autobus   = $autobus;
                $prev_privoz    = $privoz;
                $prev_trolejbus = $trolejbus;
                $prev_gondola   = $gondola;
                $prev_lanovka   = $lanovka;
            }
        }

        $dupes      = array_filter($dupes);
        $child_text = implode("','", $dupes);

        $query36 = "SELECT id FROM xmlstops where `group`='1' and id IN ('$child_text') LIMIT 1;";
        if ($result36 = mysqli_query($link, $query36)) {
            $radky36 = mysqli_num_rows($result36);
            while ($row36 = mysqli_fetch_row($result36)) {
                $group_id = $row36[0];
            }
        }
    }

    echo "Group: $group_id<br/>";
    $i       = 1;
    $query41 = "SELECT id, `name`, platform, lat, lon, `group`, linky, tramvaj, metro, vlak, autobus, privoz, trolejbus, gondola, lanovka FROM xmlstops WHERE id LIKE '$group_id/%' OR id LIKE '$group_id';";
    if ($result41 = mysqli_query($link, $query41)) {
        while ($row41 = mysqli_fetch_row($result41)) {
            $stop_id        = $row41[0];
            $stop_name      = $row41[1];
            $stop_platform  = $row41[2];
            $stop_lat       = $row41[3];
            $stop_lon       = $row41[4];
            $stop_group     = $row41[5];
            $stop_linky     = $row41[6];
            $stop_tramvaj   = $row41[7];
            $stop_metro     = $row41[8];
            $stop_vlak      = $row41[9];
            $stop_autobus   = $row41[10];
            $stop_privoz    = $row41[11];
            $stop_trolejbus = $row41[12];
            $stop_gondola   = $row41[13];
            $stop_lanovka   = $row41[14];

            echo "<tr><td>$i</td><td>";
            echo "<input type=\"checkbox\" name=\"item$i\" value=\"X$stop_id\">";
            echo "</td><td>$stop_id</td><td>$stop_name";
            echo "</td>";
            echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>$stop_group</td>";
            $pozice[] = "$stop_lat,$stop_lon";
            echo "<td>";
            if ($stop_tramvaj == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_metro == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_vlak == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_autobus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_privoz == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_trolejbus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_gondola == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_lanovka == "1") {
                echo "X";
            }
            echo "</td>";

            echo "<td>";
            echo "</td><td>$stop_linky";
            echo "</td></tr>";

            $i = $i + 1;
        }
    }

    echo "</table>";
    echo "<input type=\"submit\" name=\"action\" value=\"Merge\">";
    echo "</td><td width=\"50%\">";

    echo "<table>
    <tr><th>P.Č.</th><th>Id</th><th></th><th>Název</th><th>Nástupiště</th><th>Latitude</th><th>Longitude</th><th>Group</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th></tr>";

    $regular  = "U" . $group_id . "[A-Z]";
    $query120 = "SELECT stop_id, stop_name, stop_platform, stop_lat, stop_lon, stop_type FROM gtfsstops WHERE stop_id REGEXP '$regular';";
    if ($result120 = mysqli_query($link, $query120)) {
        while ($row120 = mysqli_fetch_row($result120)) {
            $stop_id       = $row120[0];
            $stop_name     = $row120[1];
            $stop_platform = $row120[2];
            $stop_lat      = $row120[3];
            $stop_lon      = $row120[4];
            $stop_type     = $row120[5];

            echo "<tr><td>$i</td><td>";
            echo "<input type=\"checkbox\" name=\"item$i\" value=\"G$stop_id\">";
            echo "</td><td>$stop_id</td><td>$stop_name";
            echo "</td>";
            echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>$stop_type</td>";
            $pozice[] = "$stop_lat,$stop_lon";

            unset($stop_linky_arr);
            unset($stop_druhy);
            $query134 = "SELECT route_short, route_type FROM routes WHERE route_id IN (
                SELECT DISTINCT route_id FROM trips WHERE trip_id IN (
                    SELECT DISTINCT trip_id FROM stoptimes WHERE stop_id = '$stop_id'
                )
            );";
            if ($result134 = mysqli_query($link, $query134)) {
                while ($row134 = mysqli_fetch_row($result134)) {
                    $stop_linky_arr[] = $row134[0];
                    $stop_druhy[]     = $row134[1];
                }
            }

            $stop_linky_arr = array_unique($stop_linky_arr);
            sort($stop_linky_arr);
            $stop_druhy = array_unique($stop_druhy);
            sort($stop_druhy);

            $stop_linky = implode(", ", $stop_linky_arr);

            echo "<td>";
            if (in_array("0", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("1", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("2", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("3", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("4", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("5", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("6", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if (in_array("7", $stop_druhy)) {
                echo "X";
            }
            echo "</td>";

            echo "<td>";
            echo "</td><td>$stop_linky";
            echo "</td></tr>";

            $i = $i + 1;
        }
    }

    echo "</table>";
    echo "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type=\"submit\" name=\"action\" value=\"Delete\">";
    echo "</td></tr></table>";
    echo "<input type=\"hidden\" name=\"items\" value=\"$i\">";
    echo "</form>";
    ?>
    <table width="100%"><tr>
    <td width="50%">
    <div id="m" style="height:500px"></div>
    </td><td width="50%">
    <div id="m2" style="height:500px"></div>
    </td></tr></table>
    <?php
    echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
    echo "<table>
    <tr><th>P.Č.</th><th></th><th>Id</th><th>Název</th><th>Nástupiště</th><th>Latitude</th><th>Longitude</th><th>Group</th><th>T</th><th>M</th><th>V</th><th>B</th><th>S</th><th>C</th><th>G</th><th>F</th></tr>";

    unset($pouzite_arr);
    unset($pozice2);
    $pouzite_arr[] = "'A'";
    $regular       = "U" . $group_id . "[A-Z]";
    $i             = 1;
    $query837      = "SELECT id,`name`,lat,lon,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka,`stoplist`,platform FROM stopsout WHERE id REGEXP '$regular' AND `group` = '1';";
    if ($result837 = mysqli_query($link, $query837)) {
        while ($row837 = mysqli_fetch_row($result837)) {
            $stop_id        = $row837[0];
            $stop_name      = $row837[1];
            $stop_lat       = $row837[2];
            $stop_lon       = $row837[3];
            $stop_tramvaj   = $row837[4];
            $stop_metro     = $row837[5];
            $stop_vlak      = $row837[6];
            $stop_autobus   = $row837[7];
            $stop_privoz    = $row837[8];
            $stop_trolejbus = $row837[9];
            $stop_gondola   = $row837[10];
            $stop_lanovka   = $row837[11];
            $stop_list      = $row837[12];
            $stop_platform  = $row837[13];

            echo "<tr style=\"background-color:#ddd; font-weight:bold;\"><td>$i</td><td>";
            echo "</td><td>$stop_id</td><td>$stop_name";
            echo "</td>";
            echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>1</td>";
            $pozice2[] = "$stop_lat,$stop_lon";
            echo "<td>";
            if ($stop_tramvaj == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_metro == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_vlak == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_autobus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_privoz == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_trolejbus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_gondola == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_lanovka == "1") {
                echo "X";
            }
            echo "</td></tr>";

            $i             = $i + 1;
            $stop_list_arr = explode(",", $stop_list);
            foreach ($stop_list_arr as $childstop) {
                $query909 = "SELECT id,`name`,lat,lon,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka,platform FROM stopsout WHERE id = '$childstop';";
                if ($result909 = mysqli_query($link, $query909)) {
                    while ($row909 = mysqli_fetch_row($result909)) {
                        $stop_id        = $row909[0];
                        $stop_name      = $row909[1];
                        $stop_lat       = $row909[2];
                        $stop_lon       = $row909[3];
                        $stop_tramvaj   = $row909[4];
                        $stop_metro     = $row909[5];
                        $stop_vlak      = $row909[6];
                        $stop_autobus   = $row909[7];
                        $stop_privoz    = $row909[8];
                        $stop_trolejbus = $row909[9];
                        $stop_gondola   = $row909[10];
                        $stop_lanovka   = $row909[11];
                        $stop_platform  = $row909[12];

                        echo "<tr><td>$i</td><td>";
                        echo "</td><td>$stop_id</td><td>$stop_name";
                        echo "</td>";
                        echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>0</td>";
                        $pozice2[] = "$stop_lat,$stop_lon";
                        echo "<td>";
                        if ($stop_tramvaj == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_metro == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_vlak == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_autobus == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_privoz == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_trolejbus == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_gondola == "1") {
                            echo "X";
                        }
                        echo "</td>";
                        echo "<td>";
                        if ($stop_lanovka == "1") {
                            echo "X";
                        }
                        echo "</td>";

                        echo "<td>";
                        echo "</td></tr>";

                        $i             = $i + 1;
                        $pouzite_arr[] = "'" . $stop_id . "'";
                    }
                }
            }
        }
    }

    $pouzite  = implode(",", $pouzite_arr);
    $query120 = "SELECT id,`name`,platform,lat,lon,`group`,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka FROM stopsout WHERE `group` = '0' AND id REGEXP '$regular' AND id NOT IN ($pouzite) ORDER BY platform;";
    if ($result120 = mysqli_query($link, $query120)) {
        while ($row120 = mysqli_fetch_row($result120)) {
            $stop_id        = $row120[0];
            $stop_name      = $row120[1];
            $stop_platform  = $row120[2];
            $stop_lat       = $row120[3];
            $stop_lon       = $row120[4];
            $stop_group     = $row120[5];
            $stop_tramvaj   = $row120[6];
            $stop_metro     = $row120[7];
            $stop_vlak      = $row120[8];
            $stop_autobus   = $row120[9];
            $stop_privoz    = $row120[10];
            $stop_trolejbus = $row120[11];
            $stop_gondola   = $row120[12];
            $stop_lanovka   = $row120[13];

            echo "<tr><td>$i</td><td>";
            echo "<input type=\"checkbox\" name=\"item$i\" value=\"$stop_id\">";
            echo "</td><td>$stop_id</td><td>$stop_name";
            echo "</td>";
            echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>$stop_group</td>";
            $pozice2[] = "$stop_lat,$stop_lon";
            echo "<td>";
            if ($stop_tramvaj == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_metro == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_vlak == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_autobus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_privoz == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_trolejbus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_gondola == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_lanovka == "1") {
                echo "X";
            }
            echo "</td></tr>";

            $i = $i + 1;
        }
    }
    $query120 = "SELECT id,`name`,platform,lat,lon,`group`,tramvaj,metro,vlak,autobus,privoz,trolejbus,gondola,lanovka FROM stopsout WHERE `group` = '2' AND id REGEXP '$regular' ORDER BY id;";
    if ($result120 = mysqli_query($link, $query120)) {
        while ($row120 = mysqli_fetch_row($result120)) {
            $stop_id        = $row120[0];
            $stop_name      = $row120[1];
            $stop_platform  = $row120[2];
            $stop_lat       = $row120[3];
            $stop_lon       = $row120[4];
            $stop_group     = $row120[5];
            $stop_tramvaj   = $row120[6];
            $stop_metro     = $row120[7];
            $stop_vlak      = $row120[8];
            $stop_autobus   = $row120[9];
            $stop_privoz    = $row120[10];
            $stop_trolejbus = $row120[11];
            $stop_gondola   = $row120[12];
            $stop_lanovka   = $row120[13];

            echo "<tr><td>$i</td><td>";
            echo "X";
            echo "</td><td>$stop_id</td><td>$stop_name";
            echo "</td>";
            echo "<td>$stop_platform</td><td>$stop_lat</td><td>$stop_lon</td><td>$stop_group</td>";
            $pozice2[] = "$stop_lat,$stop_lon";
            echo "<td>";
            if ($stop_tramvaj == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_metro == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_vlak == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_autobus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_privoz == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_trolejbus == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_gondola == "1") {
                echo "X";
            }
            echo "</td>";
            echo "<td>";
            if ($stop_lanovka == "1") {
                echo "X";
            }
            echo "</td></tr>";

            $i = $i + 1;
        }
    }
    echo "</table>";
    echo "<input type=\"hidden\" name=\"items\" value=\"$i\">";
    echo "<input type=\"submit\" name=\"action\" value=\"Group\">";
    echo "</form>";
    echo "<div id=\"m3\" style=\"height:0px\"></div>";
}
?>
<script type="text/javascript">
	function SelectElement(id, valueToSelect)
	{
		var element = document.getElementById(id);
		element.value = valueToSelect;
	}

	function start(e) {
		var node = e.target.getContainer();
		node[SMap.LAYER_MARKER].style.cursor = "pointer";
	}

	function stop(e) {
		var node = e.target.getContainer();
		node[SMap.LAYER_MARKER].style.cursor = "";
		var coords = e.target.getCoords();
		var souradnice = coords.toString().split(",");
		var souradnice_x = souradnice[0].replace(/\(/g,"");
		var souradnice_y = souradnice[1].replace(/\)/g,"");

		document.getElementById("latitude").value = souradnice_y;
		document.getElementById("longitude").value = souradnice_x;

		var new_center = SMap.Coords.fromWGS84(souradnice_x, souradnice_y);
		m3.setCenter(new_center);
	}

	function addMarker(nazev, id, x, y) {
		var znacka = JAK.mel("div");
		var obrazek = JAK.mel("img", {
			src: SMap.CONFIG.img + "/marker/drop-red.png"
		});
		znacka.appendChild(obrazek);

		var popisek = JAK.mel("div", {}, {
			position: "absolute",
			left: "0px",
			top: "2px",
			textAlign: "center",
			width: "22px",
			color: "white",
			fontWeight: "bold"
		});
		popisek.innerHTML = nazev;
		znacka.appendChild(popisek);


		var options = {
			title: nazev,
			url: znacka
		};

		var pozice = SMap.Coords.fromWGS84(Number(x), Number(y));
		var marker = new SMap.Marker(pozice, id, options);
		vrstva.addMarker(marker);
		markers.push(pozice);
	}

	function addMarker2(nazev, id, x, y) {
		var znacka = JAK.mel("div");
		var obrazek = JAK.mel("img", {
			src: SMap.CONFIG.img + "/marker/drop-red.png"
		});
		znacka.appendChild(obrazek);

		var popisek = JAK.mel("div", {}, {
			position: "absolute",
			left: "0px",
			top: "2px",
			textAlign: "center",
			width: "22px",
			color: "white",
			fontWeight: "bold"
		});
		popisek.innerHTML = nazev;
		znacka.appendChild(popisek);


		var options = {
			title: nazev,
			url: znacka
		};

		var pozice = SMap.Coords.fromWGS84(Number(x), Number(y));
		var marker = new SMap.Marker(pozice, id, options);
		vrstva2.addMarker(marker);
		markers2.push(pozice);
	}

	function addMarker3(nazev, id, x, y) {
		var znacka = JAK.mel("div");
		var obrazek = JAK.mel("img", {
			src: SMap.CONFIG.img + "/marker/drop-red.png"
		});
		znacka.appendChild(obrazek);

		var popisek = JAK.mel("div", {}, {
			position: "absolute",
			left: "0px",
			top: "2px",
			textAlign: "center",
			width: "22px",
			color: "white",
			fontWeight: "bold"
		});
		popisek.innerHTML = nazev;
		znacka.appendChild(popisek);


		var options = {
			title: nazev,
			url: znacka
		};

		var pozice = SMap.Coords.fromWGS84(Number(x), Number(y));
		var marker = new SMap.Marker(pozice, id, options);
		marker.decorate(SMap.Marker.Feature.Draggable);
		vrstva3.addMarker(marker);
		markers3.push(pozice);
	}


	var m = new SMap(JAK.gel("m"));
	var m2 = new SMap(JAK.gel("m2"));
    var m3 = new SMap(JAK.gel("m3"));

	m.addDefaultLayer(SMap.DEF_OPHOTO);
	m2.addDefaultLayer(SMap.DEF_OPHOTO);
	m3.addDefaultLayer(SMap.DEF_OPHOTO).enable();;
	m.addDefaultLayer(SMap.DEF_BASE).enable();
	m2.addDefaultLayer(SMap.DEF_BASE).enable();
	m3.addDefaultLayer(SMap.DEF_BASE)

	var layerSwitch = new SMap.Control.Layer({
		width: 65,
		items: 2,
		page: 2
	});
	layerSwitch.addDefaultLayer(SMap.DEF_BASE);
	layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);

	m.addControl(layerSwitch, {left:"8px", top:"9px"});
	m2.addControl(layerSwitch, {left:"8px", top:"9px"});
    m3.addControl(layerSwitch, {left:"8px", top:"9px"});

	var signals = m3.getSignals();
	signals.addListener(window, "marker-drag-stop", stop);
	signals.addListener(window, "marker-drag-start", start);

	m.addControl(new SMap.Control.Sync());
	m2.addControl(new SMap.Control.Sync());
	m3.addControl(new SMap.Control.Sync());
	var mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM);
	m.addControl(mouse);
	m2.addControl(mouse);
	m3.addControl(mouse);

    var vrstva = new SMap.Layer.Marker();
	m.addLayer(vrstva);
	vrstva.enable();
	var markers = [];

	var vrstva2 = new SMap.Layer.Marker();
	m2.addLayer(vrstva2);
	vrstva2.enable();
	var markers2 = [];

	var vrstva3 = new SMap.Layer.Marker();
	m3.addLayer(vrstva3);
	vrstva3.enable();
	var markers3 = [];

    <?php
    $k = 1;
    foreach ($pozice as $mark_bod) {
        $bod_arr = explode(",", $mark_bod);
        $bod_lat = $bod_arr[0];
        $bod_lon = $bod_arr[1];
        echo "addMarker($k, $k, $bod_lon, $bod_lat);";
        $k = $k + 1;
    }

    $k = 1;
    foreach ($pozice2 as $mark_bod) {
        $bod_arr = explode(",", $mark_bod);
        $bod_lat = $bod_arr[0];
        $bod_lon = $bod_arr[1];
        echo "addMarker2($k, $k, $bod_lon, $bod_lat);";
        $k = $k + 1;
    }

    echo "addMarker3(1, 1, $mid_lon, $mid_lat);";
    ?>

    var layerPoi = new SMap.Layer.Marker(undefined, {
		poiTooltip: true
	});
    var layerPoi2 = new SMap.Layer.Marker(undefined, {
		poiTooltip: true
	});
    var layerPoi3 = new SMap.Layer.Marker(undefined, {
		poiTooltip: true
	});
	m.addLayer(layerPoi).enable();
	m2.addLayer(layerPoi2).enable();
	m3.addLayer(layerPoi3).enable();

	var dataProvider = m.createDefaultDataProvider();
	dataProvider.setOwner(m);
	dataProvider.addLayer(layerPoi);
	dataProvider.setMapSet(SMap.MAPSET_BASE);
	dataProvider.enable();

	var dataProvider = m2.createDefaultDataProvider();
	dataProvider.setOwner(m2);
	dataProvider.addLayer(layerPoi2);
	dataProvider.setMapSet(SMap.MAPSET_BASE);
	dataProvider.enable();

	var dataProvider = m3.createDefaultDataProvider();
	dataProvider.setOwner(m3);
	dataProvider.addLayer(layerPoi3);
	dataProvider.setMapSet(SMap.MAPSET_BASE);
	dataProvider.enable();

    var cz = m.computeCenterZoom(markers);
	m.setCenterZoom(cz[0], cz[1]);

    var cz2 = m2.computeCenterZoom(markers2);
	m2.setCenterZoom(cz2[0], cz2[1]);

    var cz3 = m3.computeCenterZoom(markers3);
	m3.setCenterZoom(cz3[0], cz3[1]);
</script>