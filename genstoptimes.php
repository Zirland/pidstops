<?php
date_default_timezone_set('Europe/Prague');
ini_set('max_execution_time', 0);

require_once 'dbconnect.php';
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$dir = "PID_GTFS";

$stoptimes = fopen("$dir/stop_times.txt", 'r');
$i         = 0;
if ($stoptimes) {
    $clean_stoptimes_tab = mysqli_query($link, "TRUNCATE TABLE stoptimes;");
    while (($buffer0 = fgets($stoptimes, 4096)) !== false) {
        if ($i > 0) {
            $stp_time_line = explode(',', $buffer0);

            $trip_id = $stp_time_line[0];
            $stop_id = $stp_time_line[3];

            $query107  = "INSERT INTO stoptimes (trip_id,stop_id) VALUES ('$trip_id', '$stop_id');";
            $prikaz107 = mysqli_query($link, $query107);
            if (!$prikaz107) {
                echo "Chyba trip_id/stop_id: $trip_id - hlásí na $stop_id chybu " . mysqli_error($link) . "<br/>";
            }
        }
        $i = $i + 1;
    }
    fclose($stoptimes);
}

echo "Hotovo...";
mysqli_close($link);
